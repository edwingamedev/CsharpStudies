﻿using System;
using board;

namespace Game
{
    class Screen
    {
        public static void PrintBoard(Board board)
        {
            //Print board
            for (int i = 0; i < board.ranks; i++)
            {
                for (int j = 0; j < board.columns; j++)
                {
                    if (board.GetPiece(i, j) != null)
                    {
                        //print a piece on the board
                        Console.Write(board.GetPiece(i, j) + " ");
                    }
                    else
                    {
                        //print a blank space
                        Console.Write("- ");
                    }                                       
                }

                Console.WriteLine();
            }
        }
    }
}
