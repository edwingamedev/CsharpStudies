﻿namespace board
{
    class Position
    {
        public int rank { get; set; }
        public int column { get; set; }

        public Position(int row, int column)
        {
            this.rank = row;
            this.column = column;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}", rank, column);
        }
    }
}
