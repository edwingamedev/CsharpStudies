﻿using System;
using board;
using chess;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {
            //create a bound with given ranks and columns
            Board board = new Board(8, 8);

            board.AddPiece(new Rook(board, Color.Black), new Position(0, 0));
            board.AddPiece(new King(board, Color.Black), new Position(1, 3));

            Screen.PrintBoard(board);

            Console.ReadLine();
        }
    }
}
