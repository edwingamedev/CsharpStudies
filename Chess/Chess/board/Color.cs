﻿namespace board
{
    enum Color
    {
        White,
        Black,
        Red,
        Yellow,
        Green,
        Orange,
        Blue
    }
}
