﻿namespace board
{
    class Board
    {
        public int ranks { get; set; }
        public int columns { get; set; }
        private Piece[,] pieces;

        public Board(int ranks, int columns)
        {
            this.ranks = ranks;
            this.columns = columns;
            pieces = new Piece[ranks, columns];
        }

        public Piece GetPiece(int rank, int column)
        {
            return pieces[rank, column];
        }

        public void AddPiece(Piece piece, Position position)
        {
            //add piece to the board
            pieces[position.rank, position.column] = piece;

            //add position to the piece
            piece.position = position;
        }
    }
}
