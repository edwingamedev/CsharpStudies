﻿using System;
using System.Globalization;


namespace Class01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Type the number of accounts: ");
            int numOfAccounts = int.Parse(Console.ReadLine());

            Account[] acc = new Account[numOfAccounts];

            for (int i = 0; i < numOfAccounts; i++)
            {
                Console.WriteLine();
                Console.WriteLine("New Account");
                Console.Write("Type the account number: ");
                int number = int.Parse(Console.ReadLine());

                Console.Write("Type the name of the account owner: ");
                string owner = Console.ReadLine();

                string flag = string.Empty;

                do
                {
                    Console.Write("Will be a initial deposit (y/n)?");
                    flag = Console.ReadLine();

                    if (flag.ToLower() == "y")
                    {
                        Console.Write("Type the initial deposit amount: ");
                        acc[i] = new Account(number, owner, double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture));
                    }
                    else if (flag.ToLower() == "n")
                    {
                        acc[i] = new Account(number, owner);
                    }
                    else
                    {
                        Console.WriteLine("Invalid input.");
                    }

                } while (!flag.ToLower().Equals("y") && !flag.ToLower().Equals("n"));

                Console.WriteLine();

                Console.Write("Type a new account name: ");
                acc[i].ChangeName(Console.ReadLine());

                Console.WriteLine();

                Console.Write("Type a deposit amount: ");
                acc[i].Deposit(double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture));

                Console.WriteLine();

                Console.Write("Type a withdraw amount: ");
                acc[i].Withdraw(double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture));
            }

            Console.WriteLine();

            //show accounts
            for (int i = 0; i < numOfAccounts; i++)
            {
                Console.WriteLine("Account " + i);
                acc[i].AccountInfo();
            }

            Console.ReadLine();
        }
    }
}
