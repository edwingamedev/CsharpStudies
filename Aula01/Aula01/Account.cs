﻿using System;
using System.Globalization;

namespace Class01
{
    class Account
    {
        private int number;
        private string owner;
        private double balance;

        public void AccountInfo()
        {
            Console.WriteLine("Account: {0}, Owner: {1}, Balance: $ {2}", number, owner, balance.ToString(CultureInfo.InvariantCulture));
        }

        public Account(int number, string owner)
        {
            //acc opening without deposit
            this.number = number;
            this.owner = owner;
            this.balance = 0.00f;

            Console.WriteLine("\nAccount created:");
            AccountInfo();
        }

        public Account(int number, string owner, double deposit)
        {
            //acc opening with deposit
            this.number = number;
            this.owner = owner;
            this.balance = deposit;

            Console.WriteLine("\nAccount created:");
            AccountInfo();
        }

        public void Deposit(double amount)
        {
            //deposit amount
            balance += amount;

            Console.WriteLine(string.Format("Account Updated:\nAccount: {0}, Owner: {1}, Balance: $ {2}", number, owner, balance.ToString(CultureInfo.InvariantCulture)));
        }

        public void Withdraw(double amount)
        {
            //withdraw amount
            balance -= amount;

            //apply taxes ($5)
            balance -= 5;

            Console.WriteLine(string.Format("Account Updated:\nAccount: {0}, Owner: {1}, Balance: $ {2}", number, owner, balance.ToString()));
        }



        public void ChangeName(string name)
        {
            owner = name;

            Console.WriteLine("Owner name has changed to " + name);
        }
    }
}
